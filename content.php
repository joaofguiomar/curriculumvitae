<?php
class cvContent
{
    public function cv_manipulate_content($content)
    {
        $cat = get_the_category();
        foreach ($cat as $key => $value) {
            switch ($value->name) {
                case 'Introduction':
                    $stripped = strip_tags($content, '<img>, <p>, <b>, <i>, <a> <strong>, <em>');
                    $stripped = str_replace(
                                    array('<p>','</p>'),
                                    array('','<br/><br/>'),
                                    $stripped
                                );

                    $len = strlen($stripped);
                    $fP = substr($stripped, 0, $len/2);
                    $sP = substr($stripped, $len/2, $len);
                    $content = "<p>" . $fP . "</p><p>" . $sP . "</p>";
                    return $content ;
                    break;

                case 'Documents':
                    $stripped = strip_tags($content, '<a>');
                    $content = "<ul>";
                    $content .= preg_replace('/>(.*?)</', '><li>$1</li><', $stripped);
                    $content .= "</ul>";
                    return $content;
                    break;

                default:
                    return $content;
                    break;
            }
        }
    }
}

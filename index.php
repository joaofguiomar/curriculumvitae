<?php
    $data = get_option('cv_data');
    if ($data) {
        $name       = stripslashes($data["name"]);
        $street     = stripslashes($data["street"]);
        $country    = stripslashes($data["country"]);
        $postalcode = stripslashes($data["postalcode"]);
        $email      = stripslashes($data["email"]);
        $phone      = stripslashes($data["phone"]);
        $moto       = stripslashes($data["moto"]);
        $quote      = stripslashes($data["quote"]);
    } else {
        $name       = "John Doe";
        $street     = "Street Avenue";
        $postalcode = "3000-512 Wikipedia";
        $country    = "Universe";
        $email      = "my@email.un";
        $phone      = "+12 314 123 513";
        $moto       = "All progress takes palce outside the confort zone";
        $quote      = "I'm a brilliant mind in a busy, busy world";
    }

    //prepare quote to display
    $quoteArray = explode(' ', $quote);
    list($quote0, $quote1) = array_chunk($quoteArray, ceil(count($quoteArray)/2));
    $q0 = implode(' ', $quote0);
    $q1 = implode(' ', $quote1);

    //get OpenGraph Img URL
    $og_image  = get_theme_mod(cv_og_img);
?>
<!DOCTYPE html>
<html lang="pt_PT">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <meta property="og:title" content="<?php bloginfo('name') ?>" />
        <meta property="og:description" content="<?php bloginfo('description') ?>" />
        <meta property="og:url" content="<?php echo get_site_url(); ?>" />
        <meta property="og:image" content="<?php echo $og_image; ?>" />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="600" />

        <title><?php bloginfo('description') ?></title>
        <?php wp_head(); ?>

    </head>
    <body>
        <div id="loader"></div>

        <header>
            <div>
                <h3><?php bloginfo('name') ?></h3>
                <h1><?php echo $name; ?></h1>
                <p><?php echo $moto; ?></p>
                <a href="#contacto">
                    <button class="button"><?php _e('Contact', 'curriculumvitae'); ?></button>
                </a>
            </div>
        </header>
        <section id="introducao">
            <div>
                <ul>
                    <li>
                        <p>
                            <strong><?php echo $name; ?></strong><br>
                            <?php echo $email; ?><br>
                            <?php echo $phone; ?>
                        </p>
                    </li>
                    <li>
                    <?php
                        if (get_theme_mod('cv_avatar_img')) {
                            $image  = get_theme_mod(cv_avatar_img);
                            $id     = attachment_url_to_postid($image);
                            $alt    = get_post_meta($id, '_wp_attachment_image_alt', true);
                            echo '<img src="'.$image.'" alt="'.$alt.'">';
                        } else {
                            echo "&nbsp";
                        }
                    ?>
                    </li>
                    <li>
                        <p>
                            <strong><?php echo $street; ?></strong><br>
                            <?php echo $postalcode; ?><br>
                            <?php echo $country; ?>

                        </p>
                    </li>
                </ul>
            </div>
            <div>
                <?php
                $query_args = array(
                    'post_type'         => 'post',
                    'tax_query'         => array(
                        array(
                            'taxonomy'  => 'category',
                            'field'     => 'slug',
                            'terms'     => array( 'introduction-category' ),
                        )
                    ),
                    'order'             => 'DESC',
                    'posts_per_page'    => 1
                );

                $intro_query = new WP_Query($query_args);
                if ($intro_query->have_posts()) : while ($intro_query->have_posts()) : $intro_query->the_post();
                    the_content();
                endwhile; else:
                    echo '<h2 style="text-align: center;">';
                    _e('You should introduce yourself!', 'curriculumvitae');
                    echo "<br>";
                    _e('Add a new post and make it part of the Introduction category', 'curriculumvitae');
                    echo '</h2>';
                endif;
                ?>
            </div>
        </section>
        <section class="van">
            <div>
                <?php if (isset($q0) && isset($q1)) {
                    ?>
                <h1>
                    <span>
                        <?php echo $q0; ?>
                    </span>
                    <span>
                        <?php echo $q1; ?>
                    </span>
                </h1>
                    <?php

                } else {
                    ?>
                <h1>
                    <span>
                        <?php
                        _e('Hi! Make sure you fill ', 'curriculumvitae'); ?>
                    </span>
                    <span>
                        <?php
                        _e('the form after activating the theme', 'curriculumvitae'); ?>
                    </span>
                </h1>
                    <?php

                }?>
            </div>

        </section>
        <section id="experiencia">
            <div>
                <h1><?php _e('Professional Experience', 'curriculumvitae'); ?></h1>
                <ul>
                    <?php
                    $query_args = array(
                        'post_type' => 'cv_timeline_pt',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'cv_timeline_pt_cat',
                                'field'    => 'slug',
                                'terms'    => array( 'professional-experience-category' ),
                            )
                        ),
                        'order'     => 'ASC'
                    );

                    $pe_query = new WP_Query($query_args);
                    if ($pe_query->have_posts()) :
                        while ($pe_query->have_posts()) : $pe_query->the_post();
                            $data = get_post_meta(get_the_ID(), 'cv_timeline_data', true);
                            $to     = $data["to"];
                            $from   = date('d M Y', strtotime($data["from"]));
                            $title  = $data["title"];
                            $desc   = $data["desc"];
                            ?>
                            <li>
                                <time>
                                    <?php
                                    echo $from;
                                    $to = (isset($to) && $to != null) ? " to ".date('d M Y', strtotime($to)) : "";
                                    echo $to;
                                    ?>
                                </time>
                                <h1>
                                    <?php
                                    $title = wordwrap($title, 50, "<br>");
                                    echo $title;
                                    ?>
                                </h1>
                                <p>
                                    <?php
                                    $desc = wordwrap($desc, 50, "<br>");
                                    echo $desc;
                                    ?>
                                </p>
                            </li>
                            <?php
                        endwhile;
                    else:
                        ?>
                        <li>
                            <time>
                                <?php _e('Add some content here.', 'curriculumvitae')?>

                            </time>
                            <h1>
                                <?php _e('Hi! This sure needs some content!', 'curriculumvitae')?>

                            </h1>
                            <p>
                                <?php _e('Add items here by creating "Timeline Posts".', 'curriculumvitae')?>
                            </p>
                        </li>
                        <?php
                    endif;
                    ?>
                </ul>
            </div>
        </section>
        <section id="educacao">
            <div>
                <h1><?php _e('Education and Language', 'curriculumvitae'); ?></h1>
                <ul>
                    <?php
                    $query_args = array(
                        'post_type' => 'cv_timeline_pt',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'cv_timeline_pt_cat',
                                'field'    => 'slug',
                                'terms'    => array( 'education-category' ),
                            )
                        ),
                        'order'     => 'ASC'
                    );

                    $ed_query = new WP_Query($query_args);
                    if ($ed_query->have_posts()) :
                        while ($ed_query->have_posts()) : $ed_query->the_post();
                            $data = get_post_meta(get_the_ID(), 'cv_timeline_data', true);
                            $to     = $data["to"];
                            $from   = date('d M Y', strtotime($data["from"]));
                            $title  = $data["title"];
                            $desc   = $data["desc"];
                            ?>
                            <li>
                                <time>
                                    <?php
                                    echo $from;
                                    $to = (isset($to) && $to != null) ? "<br> to ".date('d M Y', strtotime($to)) : "";
                                    echo $to;
                                    ?>
                                </time>
                                <h1>
                                    <?php
                                    echo $title;
                                    ?>
                                </h1>
                                <p>
                                    <?php
                                    echo $desc;
                                    ?>
                                </p>
                            </li>
                            <?php
                        endwhile;
                    else:
                        ?>
                        <li>
                            <time>
                                <?php _e('Today!')?>

                            </time>
                            <h1>
                                <?php _e('Try adding a "Timeline" post', 'curriculumvitae')?>

                             </h1>
                            <p>
                                <?php _e('Make sure you choose the "education" category', 'curriculumvitae')?>

                            </p>
                        </li>
                        <?php
                    endif;
                    ?>
                </ul>
            </div>
        </section>
        <section id="aptidoes">
            <div>
                <h1><?php _e('Skill and Qualities', 'curriculumvitae'); ?></h1>
                <?php
                $query_args = array(
                    'post_type' => 'post',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'category',
                            'field'    => 'slug',
                            'terms'    => array( 'skills-category' ),
                        )
                    ),
                    'order'     => 'ASC'
                );
                $skills_query = new WP_Query($query_args);
                $count = $skills_query->post_count;
                if ($count == 1) {
                    $skills_query->the_post();
                    echo '<div style="width:95%;">';
                    the_title('<h2>', '</h2>');
                    the_content();
                    echo '</div>';
                } else {
                    $i = 0;
                    if ($skills_query->have_posts()) :
                     while ($skills_query->have_posts()) : $skills_query->the_post();
                    if ($i == 0) {
                        echo '<div>';
                    }
                    if ($i == (round($skills_query->post_count / 2))) {
                        echo '</div><div>';
                    }
                    the_title('<h2>', '</h2>');
                    the_content();
                    if ($i == round($skills_query->post_count)) {
                        echo '</div>';
                    }
                    $i++;
                    endwhile; else:
                    echo '<h2 style="text-align: center;">';
                    _e('This section needs content! Add at least two posts and make them part of the Skills category', 'curriculumvitae');
                    echo '</h2>';
                    endif;
                }
                ?>
            </div>
        </section>
        <section id="documentos">
            <div>
                <h1><?php _e('Documents', 'curriculumvitae'); ?></h1>
                <?php
                $query_args = array(
                    'post_type'         => 'post',
                    'tax_query'         => array(
                        array(
                            'taxonomy'  => 'category',
                            'field'     => 'slug',
                            'terms'     => array( 'documents-category' ),
                        )
                    ),
                    'order'             => 'ASC'
                );

                $docs_query = new WP_Query($query_args);
                if ($docs_query->have_posts()) : while ($docs_query->have_posts()) : $docs_query->the_post();
                    the_content();
                endwhile; else:
                    echo '<ul><a href="#"><li>';
                    _e('Add some documents here', 'curriculumvitae');
                    echo '</li></a>';
                    echo '<a href="#"><li>';
                    _e('Add a new post and make it part of the Documents category', 'curriculumvitae');
                    echo '</li></a></ul>';

                endif;
                ?>
            </div>
        </section>
        <section id="contacto">
            <aside><span></span></aside>
            <h1><?php _e('Interested?', 'curriculumvitae'); ?></h1>
            <h2><?php _e('Send me a message!', 'curriculumvitae'); ?></h2>
            <div>
                <input type="text" name="address" placeholder="address">
                <input type="text" name="nome" placeholder="<?php _e('Name', 'curriculumvitae'); ?>">
                <input type="text" name="email" placeholder="<?php _e('E-mail', 'curriculumvitae'); ?>">
                <textarea name="mensagem" placeholder="<?php _e('Message', 'curriculumvitae'); ?>"></textarea>
                <button id="send" class="button"><?php _e('Send message', 'curriculumvitae'); ?></button>
            </div>
        </section>
        <?php wp_footer(); ?>
    </body>
</html>

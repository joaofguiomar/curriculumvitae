# Wordpress Theme

<img src="https://gitlab.com/joaofguiomar/curriculumvitae/raw/master/screenshot.png"  width="500">

After designing and implementing my own CV site I decided to make it available as a Wordpress Theme.

Demo: http://colheitadigital.com/wordpress/


Feel free to download and use it and change it as you please.

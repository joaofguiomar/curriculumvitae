<?php
include('start.php');
include('content.php');

class CurriculumVitae
{
    public function __construct()
    {
        //Prepare for Translations
        add_action('after_setup_theme', array($this, 'cv_load_translation'));

        // Load scripts and styles
        add_action('wp_enqueue_scripts', array($this, 'cv_enqueue_scripts'));
        add_action('customize_controls_enqueue_scripts', array($this,'cv_customizer_enqueue_style'));

        // Customizer
        add_action('customize_register', array($this, 'cv_customizer_hide'));
        add_action('customize_register', array($this, 'cv_customizer'));
        add_action('wp_head', array($this,'cv_load_customized_css'));

        // Theme Settings page
        add_action('after_switch_theme', array($this, 'cv_theme_activated'), 10, 2);
        add_action('switch_theme', array($this, 'cv_theme_deactivated'));

        // Create custom categories
        add_action('after_switch_theme', array($this, 'cv_create_custom_categories'));

        // Add admin menu for theme options
        add_action('admin_init', array('cvStartPage', 'cv_admin_styles_scripts'));
        add_action('admin_post_cv_save_options', array('cvStartPage', 'cv_save_form'));
        add_action('admin_menu', array($this, 'cv_admin_menu'));
        //if settings form is saved, show success feedback
        if (isset($_GET["saved"])) {
            add_action('admin_init', array('cvStartPage','cv_success_form_saved'));
        }

        //hide admin menu
        add_action('admin_menu', array($this, 'cv_hide_admin_menu'));

        //Manipulate the_content()
        add_filter('the_content', array('cvContent', 'cv_manipulate_content'));

        //Custom Post Types
        add_action('init', array($this, 'cv_register_post_type' ));
        add_action('init', array($this, 'cv_register_custom_categories'));
        add_action('add_meta_boxes_cv_timeline_pt', array($this, 'cv_create_metabox'));
        add_action('save_post_cv_timeline_pt', array($this, 'cv_save_timeline_post'));
    }

    public function cv_hide_admin_menu()
    {
        remove_menu_page('edit.php?post_type=page');
        remove_menu_page('edit-comments.php');
    }

    public function cv_load_translation()
    {
        $domain = "curriculumvitae";
        load_theme_textdomain($domain, trailingslashit(WP_LANG_DIR) . $domain);
        load_theme_textdomain($domain, get_stylesheet_directory() . '/languages');
        load_theme_textdomain($domain, get_template_directory() . '/languages');
    }

    public function cv_save_timeline_post()
    {
        if (isset($_POST["__timeline"])) {
            $postID = $_POST["ID"];

            $timeline_data = array();
            $timeline_data["from"]  = sanitize_text_field($_POST["cv_timeline_time_from"]);
            $timeline_data["title"] = sanitize_text_field($_POST["cv_timeline_title"]);
            $timeline_data["desc"]  = sanitize_text_field($_POST["cv_timeline_desc"]);
            $timeline_data["checkbox"] = sanitize_text_field($_POST["cv_timeline_to_enable"]);

            if ($timeline_data["checkbox"] == "on") {
                $timeline_data["to"] = sanitize_text_field($_POST["cv_timeline_time_to"]);
            } else {
                $timeline_data["to"] = null;
            }
            update_post_meta($postID, 'cv_timeline_data', $timeline_data);
        }
    }

    public function cv_register_post_type()
    {
        $labels = array(
        'name'               => __('Timelines', 'curriculumvitae'),
        );
        $args = array(
        'labels'             => $labels,
        'public'             => true,
        'supports'           => 'title',
        'publicly_queryable' => false,
        'menu_position'      => 5,
        'menu_icon'          => 'dashicons-clock',
        );
        register_post_type('cv_timeline_pt', $args);
    }

    public function cv_register_custom_categories()
    {
        register_taxonomy(
            'cv_timeline_pt_cat',
            'cv_timeline_pt',
            array(
                'label' => __('Categories'),
                'hierarchical' => true
            )
        );
    }

    public function cv_create_metabox()
    {
        add_meta_box(
            'cv_timeline-metabox',
            __('Timeline Item Data', 'curriculumvitae'),
            array($this,'cv_metabox_content'),
            'cv_timeline_pt'
        );
    }

    public function cv_metabox_content($post)
    {
        wp_register_style('cv_custom_post_style', get_template_directory_uri() . '/assets/css/custom_post.css');
        wp_enqueue_style('cv_custom_post_style');
        wp_register_script('cv_custom_post_script', get_template_directory_uri() . '/assets/js/custom_post.js');
        wp_enqueue_script('cv_custom_post_script');

        if (isset($post)) {
            $timeline_data = get_post_meta(get_the_ID(), 'cv_timeline_data', true);
            $to = $timeline_data["to"];
            $from = $timeline_data["from"];
            $title = $timeline_data["title"];
            $desc = $timeline_data["desc"];
            $checked = $timeline_data["checkbox"];

            $checked = ($checked == "on") ? "checked" : "" ;
        } ?>
        <div class="metabox-section">
            <input type="hidden" name="__timeline" value="true" />

            <label for="time_from"><?php _e('from', 'curriculumvitae')?>:</label>
            <input type="date" name="cv_timeline_time_from"
            <?php
                if (isset($from)) {
                    echo 'value="'. $from.'">';
                } else {
                    echo "required >";
                } ?>
            <label for="time_to"><?php _e('to', 'curriculumvitae')?>:</label>
            <input type="date" name="cv_timeline_time_to"
            <?php

                if (isset($to) && $to != null) {
                    echo 'value="'.$to.'">';
                } else {
                    echo "disabled>";
                } ?>
            <input type="checkbox" name="cv_timeline_to_enable"
            <?php echo $checked; ?>>
        </div>
        <div class="metabox-section">
            <label for="title"><?php _e('Title', 'curriculumvitae')?></label>
            <input type="text" placeholder="Title" name="cv_timeline_title"
            <?php
                if (isset($title)) {
                    echo 'value="'. $title.'">';
                } else {
                    echo "required >";
                } ?>

            <label for="desc"><?php _e('Description', 'curriculumvitae')?></label>
            <input type="text"  placeholder="Small description" name="cv_timeline_desc"
            <?php
                if (isset($desc)) {
                    echo 'value="'. $desc.'">';
                } else {
                    echo "required >";
                } ?>
        </div>
        <?php

    }

    public function cv_create_custom_categories()
    {
        $categories = array(
            'Introduction',
            'Skills',
            'Documents'
        );
        foreach ($categories as $value) {
            if (!term_exists($value)) {
                wp_insert_term(
                    $value,
                    'category',
                    array(
                      'description'    => 'Used to add content to the ' . $value . ' section of the theme.',
                      'slug'        => strtolower($value).'-category'
                ));
            }
        }
        $categories = array(
            'Professional Experience',
            'Education'
        );
        foreach ($categories as $value) {
            if (!term_exists($value)) {
                wp_insert_term(
                    $value,
                    'cv_timeline_pt_cat',
                    array(
                      'description'    => 'Used to add content to the ' . $value . ' section of the theme.',
                      'slug'        => strtolower($value).'-category'
                ));
            }
        }
    }

    /**
     *    Settings API
     *    Create options page for this theme
     */
    //Theme Activated
     public function cv_theme_activated($oldtheme_name, $oldtheme)
     {
         if (version_compare(get_bloginfo('version'), '4.2', '<')) {
             // Switch back to previous theme
             switch_theme($oldtheme->stylesheet);
             wp_die(__("This template was only tested above 4.2. Use with caution."), 'curriculumvitae');
             return false;
         }
         $data = get_option('cv_data');
         if (!$data) {
             $arr = array(
                 'name'         =>  '',
                 'quote'        =>  '',
                 'address'      =>  '',
                 'email'        =>  '',
                 'moto'         =>  '',
             );
             add_option('cv_data', $arr);
         }

         $this->cv_set_all_customize_settings();

         wp_redirect(admin_url('admin.php?page=cv_data&first'));
     }
     //Theme Deactivated
     public function cv_theme_deactivated()
     {
         error_log("deactivated");
     }

     //Add admin menu page
     public function cv_admin_menu()
     {
         //generate the admin page menu
         add_menu_page(
             __('Tell us a bit about you.', 'curriculumvitae'),
             __('Start Here!', 'curriculumvitae'), //menu name
             'edit_themes', //priviledge
             'cv_data', //page name (url)
             array('cvStartPage', 'cv_data_page'),
             '',
             99 // menu location
         );
     }

    /**
     *    Customizer API
     *    Custom panels, settings and options for this theme
     */
    //Hide default Customize API Panels and Sections
    public function cv_customizer_hide($wp_customize)
    {
        $wp_customize->remove_section('static_front_page');
        $wp_customize->remove_section('custom_css');
         #Remove "Menus" Section from Customizer
         remove_action('customize_controls_enqueue_scripts', array( $wp_customize->nav_menus, 'enqueue_scripts' ));
        remove_action('customize_register', array( $wp_customize->nav_menus, 'customize_register' ), 11);
    }

    //set all default values for customizer settings
    public function cv_set_all_customize_settings()
    {
        set_theme_mod('cv_og_img', get_template_directory_uri() . '/assets/img/thumb.png');
        set_theme_mod('cv_buttons_color', '#FEFEFE');
        set_theme_mod('cv_buttons_hover_color', '#dd9d1c');
        set_theme_mod('cv_avatar_img', get_template_directory_uri() . '/assets/img/avatar.jpg');
        set_theme_mod('cv_header_img', get_template_directory_uri() . '/assets/img/header.jpg');
        set_theme_mod('cv_header_text_color', '#FEFEFE');
        set_theme_mod('cv_introduction_section_p_color', '#747474');
        set_theme_mod('cv_introduction_section_title_color', '#0064a8');
        set_theme_mod('cv_first_section_img', get_template_directory_uri() . '/assets/img/docs_section.jpg');
        set_theme_mod('cv_van_section_title_color', '#FEFEFE');
        set_theme_mod('cv_van_section_title_bg_color', '#0064a8');
        set_theme_mod('cv_prof_exp_section_title_color', '#0064a8');
        set_theme_mod('cv_prof_exp_section_item_date_color', '#BCBCBC');
        set_theme_mod('cv_prof_exp_section_item_title_color', '#747474');
        set_theme_mod('cv_prof_exp_section_item_desc_color', '#0064a8');
        set_theme_mod('cv_prof_exp_section_timeline_color', '#0064a8');
        set_theme_mod('cv_ed_lang_section_text_color', '#FEFEFE');
        set_theme_mod('cv_ed_lang_section_bg_color', '#0064a8');
        set_theme_mod('cv_ed_lang_section_title_color', '#FEFEFE');
        set_theme_mod('cv_ed_lang_section_timeline_color', '#FEFEFE');
        set_theme_mod('cv_skills_section_title_color', '#0064a8');
        set_theme_mod('cv_skills_section_text_color', '#747474');
        set_theme_mod('cv_documents_section_img', get_template_directory_uri() . '/assets/img/docs_section.jpg');
        set_theme_mod('cv_documents_section_title_color', '#0064a8');
        set_theme_mod('cv_documents_section_text_color', '#FEFEFE');
        set_theme_mod('cv_documents_section_bg_color', '#0064a8');
        set_theme_mod('cv_footer_section_title_color', 'FEFEFE');
        set_theme_mod('cv_footer_section_bg_color', '#009DF4');
    }

    //Custom Customize API Sections Settings and Controls
    public function cv_customizer($wp_customize)
    {
        /**************************************
         *
         *     PANELS ARE CREATED HERE
         *
         **************************************/
        $wp_customize->add_panel('cv_theme_options_panel', array(
            'title'             => __('Theme Options', 'curriculumvitae'),
            'description'       => __('Change colors, images and other styles')
        ));

        /**************************************
         *
         *     SECTIONS ARE CREATED HERE
         *
         **************************************/
        $wp_customize->add_section('cv_section_colors', array(
            'title'             => __('Colors', 'curriculumvitae'),
            'description'       => __('Apply your own custom colors.', 'curriculumvitae'),
            'panel'             => 'cv_theme_options_panel'
        ));
        $wp_customize->add_section('cv_section_images', array(
            'title'             => __('Images', 'curriculumvitae'),
            'description'       => __('Change images.', 'curriculumvitae'),
            'panel'             => 'cv_theme_options_panel'
        ));
        $wp_customize->add_section('cv_section_form', array(
            'title'             => __('Contact Form', 'curriculumvitae'),
            'description'       => __('The contact form uses Gmail SMTP. I would recomend creating a password just for this. Do this at "https://passwords.google.com/"', 'curriculumvitae'),
            'panel'             => 'cv_theme_options_panel'
        ));


        /**************************************
         *
         *     CONTACT FORM
         *     CONTROLS AND SETTIGNS
         *
         **************************************/

         //Form To: Name
        $wp_customize->add_setting('cv_form_name', array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_text_field'
        ));
        $wp_customize->add_control(new WP_Customize_Control(
             $wp_customize,
             'cv_form_name',
             array(
                 'label'        => __('Name', 'curriculumvitae'),
                 'section'      => 'cv_section_form',
                 'setting'      => 'cv_form_name'
             )
         ));

        //Form To: e-mail
        $wp_customize->add_setting('cv_form_email', array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_email'
        ));
        $wp_customize->add_control(new WP_Customize_Control(
             $wp_customize,
             'cv_form_email',
             array(
                 'label'        => __('E-mail', 'curriculumvitae'),
                 'section'      => 'cv_section_form',
                 'setting'      => 'cv_form_email'
             )
         ));

         //Form e-mail password for gmail auth
         $wp_customize->add_setting('cv_form_password', array(
             'default'           => '',
             'sanitize_callback' => 'sanitize_text_field'
         ));
        $wp_customize->add_control(new WP_Customize_Control(
              $wp_customize,
              'cv_form_password',
              array(
                  'type'         => 'password',
                  'label'        => __('Password', 'curriculumvitae'),
                  'section'      => 'cv_section_form',
                  'setting'      => 'cv_form_password'
              )
          ));

        //Form Subject field
        $wp_customize->add_setting('cv_form_subject', array(
            'default'           => '',
            'sanitize_callback' => 'sanitize_text_field'
        ));
        $wp_customize->add_control(new WP_Customize_Control(
             $wp_customize,
             'cv_form_subject',
             array(
                 'label'        => __('Subject', 'curriculumvitae'),
                 'section'      => 'cv_section_form',
                 'setting'      => 'cv_form_subject'
             )
         ));

        /**************************************
         *
         *     IMAGE SECTION
         *     CONTROLS AND SETTIGNS
         *
         **************************************/
         //OG Image
          $wp_customize->add_setting('cv_og_img', array(
              'default' => get_template_directory_uri() . '/assets/img/thumb.png',
          ));
        $wp_customize->add_control(new WP_Customize_Image_Control(
              $wp_customize,
              'cv_og_img',
              array(
                  'label'        => __('OpenGraph Image', 'curriculumvitae'),
                  'section'      => 'cv_section_images',
                  'setting'      => 'cv_og_img'
              )
          ));
        //Header Image
         $wp_customize->add_setting('cv_header_img', array(
             'default' => get_template_directory_uri() . '/assets/img/header.jpg'
         ));
        $wp_customize->add_control(new WP_Customize_Image_Control(
             $wp_customize,
             'cv_header_img',
             array(
                 'label'        => __('Header Background Image', 'curriculumvitae'),
                 'section'      => 'cv_section_images',
                 'setting'      => 'cv_header_img'
             )
         ));
         //Avatar Image
          $wp_customize->add_setting('cv_avatar_img', array(
              'default' => get_template_directory_uri() . '/assets/img/avatar.jpg'
          ));
        $wp_customize->add_control(new WP_Customize_Image_Control(
              $wp_customize,
              'cv_avatar_img',
              array(
                  'label'        => __('Avatar Image', 'curriculumvitae'),
                  'section'      => 'cv_section_images',
                  'setting'      => 'cv_avatar_img'
              )
          ));
         //First Section Images
         $wp_customize->add_setting('cv_first_section_img', array(
             'default' => get_template_directory_uri() . '/assets/img/section.jpg',
         ));
        $wp_customize->add_control(new WP_Customize_Image_Control(
             $wp_customize,
             'cv_first_section_img',
             array(
                 'label'        => __('First Section Image', 'curriculumvitae'),
                 'section'      => 'cv_section_images',
                 'setting'      => 'cv_first_section_img'
             )
         ));

         //Document Section Image
         $wp_customize->add_setting('cv_documents_section_img', array(
             'default' => get_template_directory_uri() . '/assets/img/docs_section.jpg',
         ));
        $wp_customize->add_control(new WP_Customize_Image_Control(
             $wp_customize,
             'cv_documents_section_img',
             array(
                 'label'        => __('Documents Section Image', 'curriculumvitae'),
                 'section'      => 'cv_section_images',
                 'setting'      => 'cv_documents_section_img'
             )
         ));

         /**************************************
          *
          *     COLOR SECTION
          *     CONTROLS AND SETTINGS
          *
          **************************************/
        //Header Text Color
        $wp_customize->add_setting('cv_header_text_color', array(
            'default'           => '#FEFEFE',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_header_text_color',
             array(
                'label'         => __('Header Text Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_header_text_color'
            )
        ));

        //Button Color
        $wp_customize->add_setting('cv_buttons_color', array(
            'default'           => '#FEFEFE',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_buttons_color',
             array(
                'label'         => __('Buttons Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_buttons_color'
            )
        ));

        //Button Color
        $wp_customize->add_setting('cv_buttons_hover_color', array(
            'default'           => '#dd9d1c',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_buttons_hover_color',
             array(
                'label'         => __('Buttons Hover Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_buttons_hover_color'
            )
        ));

        //Introduction Section
        //Title colors
        $wp_customize->add_setting('cv_introduction_section_title_color', array(
            'default'           => '#0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_introduction_section_title_color',
             array(
                'label'         => __('Introduction Title Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_introduction_section_title_color'
            )
        ));
        //Text Color
        $wp_customize->add_setting('cv_introduction_section_p_color', array(
            'default'           => '#747474',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_introduction_section_p_color',
             array(
                'label'         => __('Introduction Paragraph Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_introduction_section_p_color'
            )
        ));


        //Van Section
        //Title colors
        $wp_customize->add_setting('cv_van_section_title_color', array(
            'default'           => '#FEFEFE',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_van_section_title_color',
             array(
                'label'         => __('First Separator Title color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_van_section_title_color'
            )
        ));

        //Title Background Color
        $wp_customize->add_setting('cv_van_section_title_bg_color', array(
            'default'           => '#0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_introduction_section_title_bg_color',
             array(
                'label'         => __('First Separator Title Background Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_van_section_title_bg_color'
            )
        ));

        //Professional Experience Section
        //Title color
        $wp_customize->add_setting('cv_prof_exp_section_title_color', array(
            'default'           => '#0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_prof_exp_section_title_color',
             array(
                'label'         => __('Professional Experience Title color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_prof_exp_section_title_color'
            )
        ));
        //Timeline Item Title color
        $wp_customize->add_setting('cv_prof_exp_section_item_title_color', array(
            'default'           => '#747474',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_prof_exp_section_item_title_color',
             array(
                'label'         => __('Timeline Item Title color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_prof_exp_section_item_title_color'
            )
        ));
        //Timeline Item Date color
        $wp_customize->add_setting('cv_prof_exp_section_item_date_color', array(
            'default'           => '#BCBCBC',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_prof_exp_section_item_date_color',
             array(
                'label'         => __('Timeline Item Date color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_prof_exp_section_item_date_color'
            )
        ));
        //Timeline Item Description color
        $wp_customize->add_setting('cv_prof_exp_section_item_desc_color', array(
            'default'           => '#0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_prof_exp_section_item_desc_color',
             array(
                'label'         => __('Timeline Item Description color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_prof_exp_section_item_desc_color'
            )
        ));
        //Timeline Stroke color
        $wp_customize->add_setting('cv_prof_exp_section_timeline_color', array(
            'default'           => '#0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_prof_exp_section_timeline_color',
             array(
                'label'         => __('Timeline Stroke Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_prof_exp_section_timeline_color'
            )
        ));



        //Education and Language Section
        //Title color
        $wp_customize->add_setting('cv_ed_lang_section_title_color', array(
            'default'           => '#FEFEFE',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_ed_lang_section_title_color',
             array(
                'label'         => __('Education and Language Title Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_ed_lang_section_title_color'
            )
        ));
        //Background color
        $wp_customize->add_setting('cv_ed_lang_section_bg_color', array(
            'default'           => '#0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_ed_lang_section_bg_color',
             array(
                'label'         => __('Education and Language Background Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_ed_lang_section_bg_color'
            )
        ));
        //Text color
        $wp_customize->add_setting('cv_ed_lang_section_text_color', array(
            'default'           => '#FEFEFE',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_ed_lang_section_text_color',
             array(
                'label'         => __('Education and Language Text Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_ed_lang_section_text_color'
            )
        ));
        //Timeline color
        $wp_customize->add_setting('cv_ed_lang_section_timeline_color', array(
            'default'           => '#FEFEFE',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_ed_lang_section_timeline_color',
             array(
                'label'         => __('Education and Language Timeline Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_ed_lang_section_timeline_color'
            )
        ));

        //Skills Section
        //Title color
        $wp_customize->add_setting('cv_skills_section_title_color', array(
            'default'           => '#0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_skills_section_title_color',
             array(
                'label'         => __('Skills Title Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_skills_section_title_color'
            )
        ));
        //Text color
        $wp_customize->add_setting('cv_skills_section_text_color', array(
            'default'           => '#747474',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_skills_section_text_color',
             array(
                'label'         => __('Skills Text Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_skills_section_text_color'
            )
        ));

        //Documents Section
        //Title color
        $wp_customize->add_setting('cv_documents_section_title_color', array(
            'default'           => '#0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_documents_section_title_color',
             array(
                'label'         => __('Documents Title Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_documents_section_title_color'
            )
        ));
        //Text color
        $wp_customize->add_setting('cv_documents_section_text_color', array(
            'default'           => '#FEFEFE',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_documents_section_text_color',
             array(
                'label'         => __('Documents Text Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_documents_section_text_color'
            )
        ));
        //Background color
        $wp_customize->add_setting('cv_documents_section_bg_color', array(
            'default'           => '#0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_documents_section_bg_color',
             array(
                'label'         => __('Documents Background Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_documents_section_bg_color'
            )
        ));

        //Footer Section
        //Title color
        $wp_customize->add_setting('cv_footer_section_title_color', array(
            'default'           => '#FEFEFE',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_footer_section_title_color',
             array(
                'label'         => __('Footer Title Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_footer_section_title_color'
            )
        ));
        //Background color
        $wp_customize->add_setting('cv_footer_section_bg_color', array(
            'default'           => '##0064a8',
            'sanitize_callback' => 'sanitize_hex_color',
        ));
        $wp_customize->add_control(new WP_Customize_Color_Control(
             $wp_customize,
             'cv_footer_section_bg_color',
             array(
                'label'         => __('Custom Footer Background Color', 'curriculumvitae'),
                'section'       => 'cv_section_colors',
                'settings'      => 'cv_footer_section_bg_color',
            )
        ));
    }

    public function cv_load_customized_css()
    {
        ?>
        <style type="text/css">
            img {max-width:100%; height: auto;}
            .button{
                color: <?php echo get_theme_mod('cv_buttons_color'); ?>; border: 1px solid <?php echo get_theme_mod('cv_buttons_color'); ?>;
            }
            .button:hover{

                background-color: <?php echo get_theme_mod('cv_buttons_hover_color'); ?>;
                border: 1px solid <?php echo get_theme_mod('cv_buttons_hover_color'); ?>;
                color: <?php echo get_theme_mod('cv_buttons_color'); ?>
            }
            body>header{background-image:<?php echo "url('" . get_theme_mod('cv_header_img') . "');"?>;}
            header h3, header p, header h1{color:<?php echo get_theme_mod('cv_header_text_color'); ?>;}

            #introducao div p { color:<?php echo get_theme_mod('cv_introduction_section_p_color'); ?>;}
            #introducao div a { color:<?php echo get_theme_mod('cv_introduction_section_title_color'); ?>;}
            #introducao ul li p { color:<?php echo get_theme_mod('cv_introduction_section_title_color'); ?>;}
            #introducao>div:first-child img {
                border: 2px solid <?php echo get_theme_mod('cv_introduction_section_title_color'); ?>;
            }

            .van{background-image:<?php echo "url('" . get_theme_mod('cv_first_section_img') . "');"?>;}
            .van h1{ color:<?php echo get_theme_mod('cv_van_section_title_color'); ?>;}
            .van>div>h1>span{ background-color:<?php echo get_theme_mod('cv_van_section_title_bg_color'); ?>;}

            #experiencia>div>h1 { color:<?php echo get_theme_mod('cv_prof_exp_section_title_color'); ?>;}
            #experiencia ul li time { color:<?php echo get_theme_mod('cv_prof_exp_section_item_date_color'); ?>;}
            #experiencia ul li h1 { color:<?php echo get_theme_mod('cv_prof_exp_section_item_title_color'); ?>;}
            #experiencia ul li p { color:<?php echo get_theme_mod('cv_prof_exp_section_item_desc_color'); ?>;}
            #experiencia ul::after {background-color:<?php echo get_theme_mod('cv_prof_exp_section_timeline_color'); ?>;}
            #experiencia ul::before {border-color:<?php echo get_theme_mod('cv_prof_exp_section_timeline_color'); ?> transparent transparent transparent;}
            #experiencia ul li:nth-child(even)::before {background-color:<?php echo get_theme_mod('cv_prof_exp_section_timeline_color'); ?>;}
            #experiencia ul li:nth-child(odd)::before {background-color:<?php echo get_theme_mod('cv_prof_exp_section_timeline_color'); ?>;}
            #experiencia ul li:nth-child(even) { border-left: 1px dashed<?php echo get_theme_mod('cv_prof_exp_section_timeline_color'); ?>;}
            #experiencia ul li:nth-child(odd) { border-right: 1px dashed<?php echo get_theme_mod('cv_prof_exp_section_timeline_color'); ?>;}


            #educacao {color:<?php echo get_theme_mod('cv_ed_lang_section_text_color')?>; background-color:<?php echo get_theme_mod('cv_ed_lang_section_bg_color')?>;}
            #educacao>div>h1 {color:<?php echo get_theme_mod('cv_ed_lang_section_title_color')?>;}
            #educacao>div>ul:nth-child(2)::before {border-color:<?php echo get_theme_mod('cv_ed_lang_section_timeline_color')?> transparent transparent transparent;}
            #educacao>div>ul:nth-child(2) li { border-left: 1px dashed <?php echo get_theme_mod('cv_ed_lang_section_timeline_color')?>;}
            #educacao>div>ul:nth-child(2)::after{background-color:<?php echo get_theme_mod('cv_ed_lang_section_timeline_color')?>;}

            #aptidoes{color:<?php echo get_theme_mod('cv_skills_section_title_color')?>;}
            #aptidoes>div>div>p {color:<?php echo get_theme_mod('cv_skills_section_text_color')?>;}
            #aptidoes li {color:<?php echo get_theme_mod('cv_skills_section_title_color')?>;
            font-size: 1.8rem;}
            #aptidoes a {color:<?php echo get_theme_mod('cv_skills_section_title_color')?>;
            }

            #documentos{background-image:<?php echo "url('" . get_theme_mod('cv_documents_section_img') . "');"?>;}
            #documentos>div>h1 {color:<?php echo get_theme_mod('cv_documents_section_title_color') ?>;}
            #documentos a { color:<?php echo get_theme_mod('cv_documents_section_text_color') ?>; }
            #documentos>div>ul>a>li{ background-color:<?php echo get_theme_mod('cv_documents_section_bg_color') ?>;}
            #documentos>div>ul>a>li::before{ background-color:<?php echo get_theme_mod('cv_documents_section_bg_color') ?>;}

            #contacto>h1, #contacto>h2{ color:<?php echo get_theme_mod('cv_footer_section_title_color') ?>;}

            #contacto {background-image: none; background-color:<?php echo get_theme_mod('cv_footer_section_bg_color')?>;}


        @media only screen and (max-width: 440px){
            #experiencia ul li:nth-child(odd) {
                margin-left: 40px;
                text-align: left;
                width: 85%;
                border-left: 1px dashed <?php echo get_theme_mod('cv_prof_exp_section_timeline_color'); ?>;
                border-right: none;
            }
        }
        </style>
        <?php

    }

    /*****************************************************
     *  Register and Enqueue all scritps and style files
     */
     public function cv_customizer_enqueue_style()
     {
         wp_register_style('cv_customizer_style', get_template_directory_uri() . '/assets/css/customizer.css');
         wp_enqueue_style('cv_customizer_style');
     }

    public function cv_enqueue_scripts()
    {
        //register styles
        wp_register_style('cv_main', get_template_directory_uri() . '/assets/css/main.min.css');
        wp_register_style('cv_normalize', get_template_directory_uri() . '/assets/css/normalize.min.css');
        wp_register_style('cv_font', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,700');

        // register script
        wp_register_script('cv_main', get_template_directory_uri() . '/assets/js/main.min.js');

        //enqueue styles
        wp_enqueue_style('cv_normalize');
        wp_enqueue_style('cv_font');
        wp_enqueue_style('cv_main');

        //enqueue scrips
        wp_enqueue_script('jquery');
        wp_enqueue_script('cv_main');

        //send template directory as an object to main.js (cv_main)
        $send_to_js = array('templateURI' => get_stylesheet_directory_uri());
        wp_localize_script('cv_main', 'templatePath', $send_to_js);
    }
}

$curriculumvitae = new CurriculumVitae();

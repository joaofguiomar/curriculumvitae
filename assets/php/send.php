<?php
require '../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
require '../../../../../wp-load.php';

$WP_name = get_theme_mod('cv_form_name');
$WP_email = get_theme_mod('cv_form_email');
$WP_subject = get_theme_mod('cv_form_subject');
$WP_password = get_theme_mod('cv_form_password');


if (isset($_POST["name_unfiltered"]) &&
    isset($_POST["email_unfiltered"]) &&
    isset($_POST["message_unfiltered"])) {
    $name_unfiltered = $_POST["name_unfiltered"];
    $email_unfiltered = $_POST["email_unfiltered"];
    $message_unfiltered = $_POST["message_unfiltered"];

    $name = filter_var($name_unfiltered, FILTER_SANITIZE_STRING);
    $email = filter_var($email_unfiltered, FILTER_SANITIZE_EMAIL);
    $message = filter_var($message_unfiltered, FILTER_SANITIZE_STRING);
    $message = nl2br($message);

    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->CharSet = 'UTF-8';
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = $WP_email;
    $mail->Password = $WP_password;

    $text = file_get_contents('email_template/email.html');
    $text = str_replace('%name%', $name, $text);
    $text = str_replace('%email%', $email, $text);
    $text = str_replace('%message%', $message, $text);

    $mail->setFrom($email, $name);
    $mail->addReplyTo($email, $email);
    $mail->addAddress($WP_email, $WP_name);
    $mail->Subject = $WP_subject;
    $mail->MsgHTML($text);

    if (!$mail->send()) {
        header('HTTP/1.1 500 Internal Server Error');
        header('Content-Type: application/json; charset=UTF-8');
        die(json_encode(array('message' => 'ERROR', 'code' => 500)));
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        return;
    }
}

jQuery(document).ready(function ($){
    //jQuery('input, label, button, h1,h2,h3').addClass('reveal');
    jQuery('input,label,button,h1,h2,h3').each(function(i, el){
        jQuery(el).delay(i*80).queue(function(next){
            jQuery(el).addClass('reveal');
            next();
        });
    });
});

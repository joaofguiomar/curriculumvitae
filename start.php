<?php
class cvStartPage
{
    public function cv_save_form()
    {
        if (!current_user_can('edit_themes')) {
            //check for user permissions
            wp_die(__("You don't have permissions for this", "curriculumvitae"));
        }
        //check for nonce used in form
        check_admin_referer('cv_options_verify');

        $data = get_option("cv_data");
        $data["name"]       = sanitize_text_field($_POST["cv_name"]);
        $data["street"]     = sanitize_text_field($_POST["cv_street"]);
        $data["postalcode"] = sanitize_text_field($_POST["cv_postalcode"]);
        $data["country"]    = sanitize_text_field($_POST["cv_country"]);
        $data["email"]      = sanitize_text_field($_POST["cv_email"]);
        $data["phone"]      = sanitize_text_field($_POST["cv_phone"]);
        $data["quote"]      = sanitize_text_field($_POST["cv_quote"]);
        $data["moto"]       = sanitize_text_field($_POST["cv_moto"]);

        update_option('cv_data', $data);
        $url = "admin.php?page=cv_data&saved";
        wp_redirect(admin_url($url));
    }
    public function cv_success_form_saved()
    {
        $custom_css = '
               .admin input[type=text]{
                       color: #6bd07e;
               }';
        wp_add_inline_style('cv_admin_style', $custom_css);
    }

    public function cv_data_page()
    {
        $data = get_option('cv_data'); ?>
            <div class="admin">
                <?php
                if (isset($_GET["first"])) {
                    ?>
                    <h1><?php _e('Welcome!', 'curriculumvitae'); ?></h1>
                    <h1><?php _e('Tell us a bit about you', 'curriculumvitae'); ?>.</h2>
                    <h3><?php _e('These fields will be used to fill the template with your information', 'curriculumvitae'); ?>.</h3>
                    <?php

                } ?>

                <form action="admin-post.php" method="post">
                    <input type="hidden" name="action" value="cv_save_options">
                    <?php wp_nonce_field('cv_options_verify'); ?>

                    <label for="cv_name">
                        <?php _e('What\'s your name?', 'curriculumvitae'); ?>
                        </label>
                    <input type="text" name="cv_name" value="<?php echo stripslashes($data["name"]); ?>">

                    <label for="cv_name"><?php _e('What\'s your address?', 'curriculumvitae'); ?></label>
                    <input type="text" name="cv_street" value="<?php echo stripslashes($data["street"]); ?>">

                    <label for="cv_name">
                        <?php _e('What\'s your postal code?', 'curriculumvitae'); ?>

                    </label>
                    <input type="text" name="cv_postalcode" value="<?php echo stripslashes($data["postalcode"]); ?>">

                    <label for="cv_name">
                        <?php _e('What\'s your country?', 'curriculumvitae'); ?>
                    </label>
                    <input type="text" name="cv_country" value="<?php echo stripslashes($data["country"]); ?>">

                    <label for="cv_name">
                        <?php _e('What\'s your e-mail address?', 'curriculumvitae'); ?>
                    </label>
                    <input type="text" name="cv_email" value="<?php echo stripslashes($data["email"]); ?>">

                    <label for="cv_name">
                        <?php _e('What\'s your phone number?', 'curriculumvitae'); ?>
                    </label>
                    <input type="text" name="cv_phone" value="<?php echo stripslashes($data["phone"]); ?>">

                    <label for="cv_name">
                        <?php _e('What\'s your favourite quote?', 'curriculumvitae'); ?>
                    </label>
                    <input type="text" name="cv_quote" value="<?php echo stripslashes($data["quote"]); ?>">

                    <label for="cv_name">
                        <?php _e('What\'s your life moto?', 'curriculumvitae'); ?>
                    </label>
                    <input type="text" name="cv_moto" value="<?php echo stripslashes($data["moto"]); ?>">
                    <button type="submit"><?php _e('Save Options', 'curriculumvitae'); ?></button>
                </form>
            </div>
        <?php

    }
    public function cv_admin_styles_scripts()
    {
        if (!isset($_GET['page']) || $_GET['page'] != 'cv_data') {
            return true;
        }
        wp_register_style('cv_admin_style', get_template_directory_uri() . '/assets/css/admin.css');
        wp_register_script('cv_admin_script', get_template_directory_uri() . '/assets/js/admin.js');
        wp_enqueue_style('cv_admin_style');
        wp_enqueue_script('cv_admin_script');
    }
}
